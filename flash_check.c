#include "msp430.h"
#include "hal_lcd.h"

char val[]="XXXX\0";
int conv(int adc_val)
{    unsigned char thousands = '0'; 
	 unsigned char hundreds = '0';           // Init to '0' to code in ASCII
    unsigned char tens = '0';
    unsigned char ones = '0';
   while (adc_val >= 1000){                 // Calculate hundreds' place
       thousands++;
        adc_val -= 1000;
    }
    while (adc_val >= 100){                 // Calculate hundreds' place
        hundreds++;
        adc_val -= 100;
    }
  // if (hundreds == '0') hundreds = ' ';    //eliminate preceding zero, if any
    while (adc_val >= 10){                  // Calculate tens' place
        tens++;
        adc_val -= 10;
    }
    ones += adc_val; 
    val[3]= ones;
    val[2]=tens;
    val[1]=hundreds;
    val[0]=thousands;
    return 0;
}

  int count=1;
 int flag=1,flag0=1,flag1=1,flag2=1;
  int prev=0,page=0;
  char *k,*l,*m;
void initi()
{
	P2DIR=0x00;
	
	P2REN|=0xFF;
	P2OUT|=0xFF;
	P2IE|=0xFF;
	P2IES|=0xFF;
	
	P1DIR|=0x03;
	P1OUT|=0x03;
//	TA1CCTL0 = CCIE;                          // CCR0 interrupt enabled
//  TA1CCR0 = 65535;
//  TA1CTL = TASSEL_1 + MC_1 + TACLR+ID_3;         // SMCLK, upmode, clear TAR
//	
}
void menu_initi(int a)
{
	    halLcdClearScreen();
halLcdPrintLine("Solar system:", 3, OVERWRITE_TEXT);
halLcdPrintLine("1.Clock", 4, OVERWRITE_TEXT);
halLcdPrintLine("2.SOC", 5,  OVERWRITE_TEXT);
halLcdPrintLine("3.Battery", 6, OVERWRITE_TEXT);

switch(a)
{
	case 1:
	halLcdPrintLine("1.Clock", 4, INVERT_TEXT|OVERWRITE_TEXT);
	break;
	case 2:
	halLcdPrintLine("2.SOC", 5, INVERT_TEXT|OVERWRITE_TEXT);
	break;
	case 3:
	halLcdPrintLine("3.Battery", 6, INVERT_TEXT|OVERWRITE_TEXT);
	break;
}
 
}	

void SetVcoreUp (unsigned int level)
{
  // Open PMM registers for write
  PMMCTL0_H = PMMPW_H;              
  // Set SVS/SVM high side new level
  SVSMHCTL = SVSHE + SVSHRVL0 * level + SVMHE + SVSMHRRL0 * level;
  // Set SVM low side to new level
  SVSMLCTL = SVSLE + SVMLE + SVSMLRRL0 * level;
  // Wait till SVM is settled
  while ((PMMIFG & SVSMLDLYIFG) == 0);
  // Clear already set flags
  PMMIFG &= ~(SVMLVLRIFG + SVMLIFG);
  // Set VCore to new level
  PMMCTL0_L = PMMCOREV0 * level;
  // Wait till new level reached
  if ((PMMIFG & SVMLIFG))
    while ((PMMIFG & SVMLVLRIFG) == 0);
  // Set SVS/SVM low side to new level
  SVSMLCTL = SVSLE + SVSLRVL0 * level + SVMLE + SVSMLRRL0 * level;
  // Lock PMM registers for write access
  PMMCTL0_H = 0x00;
}
void freq_setup()
{
	 SetVcoreUp (0x01);
 SetVcoreUp (0x02);  
 SetVcoreUp (0x03);
	UCSCTL3 = SELREF_2;                       // Set DCO FLL reference = REFO
  UCSCTL4 |= SELA_2;  
  UCSCTL0 = 0x0000;                         // Set lowest possible DCOx, MODx
  UCSCTL1 = DCORSEL_5;                      // Select DCO range 50MHz operation
  UCSCTL2 = FLLD_1 + 762;                       // Set ACLK = REFO
   do
  {
    UCSCTL7 &= ~(XT2OFFG + XT1LFOFFG + XT1HFOFFG + DCOFFG);
                                            // Clear XT2,XT1,DCO fault flags
    SFRIFG1 &= ~OFIFG;                      // Clear fault flags
  }while (SFRIFG1&OFIFG);   
}
void clockpage()
{
	halLcdClearScreen();
  halLcdPrintLine("clockpage", 5, OVERWRITE_TEXT);
  flag0=0;
  flag1=flag2=0;
  unsigned long * Flash_ptrD;               // Initialize Flash pointer Seg D
  unsigned long * Flash_ptrE;
  unsigned long * Flash_ptrF;
  unsigned long * Flash_ptrG;
  unsigned long * Flash_ptrH;
  unsigned long * Flash_ptrI;
  unsigned long * Flash_ptrJ;
  unsigned long value;


  Flash_ptrD = (unsigned long *) 0x1800;    // Initialize Flash pointer
   Flash_ptrE = (unsigned long *) 0x2000; 
    Flash_ptrF = (unsigned long *) 0x2200; 
     Flash_ptrG = (unsigned long *) 0x2400; 
      Flash_ptrH = (unsigned long *) 0x2600; 
       Flash_ptrI = (unsigned long *) 0x2800; 
        Flash_ptrJ = (unsigned long *) 0x3800; 
  value = 15;                       // Initialize Value
  //__disable_interrupt();                    // 5xx Workaround: Disable global
                                            // interrupt while erasing. Re-Enable
                                            // GIE if needed
  FCTL3 = FWKEY;                            // Clear Lock bit
  //FCTL1 = FWKEY+ERASE;                      // Set Erase bit
  //*Flash_ptrD = 0;                          // Dummy write to erase Flash seg
  FCTL1 = FWKEY+BLKWRT;                     // Enable long-word write
  *Flash_ptrD = value; 
  
  FCTL1 = FWKEY;                            // Clear WRT bit
  FCTL3 = FWKEY+LOCK;                     // Write to Flash
    FCTL1 = FWKEY+BLKWRT;
  *Flash_ptrE =value++;
  
  FCTL1 = FWKEY;                            // Clear WRT bit
  FCTL3 = FWKEY+LOCK;
    FCTL1 = FWKEY+BLKWRT;
  *Flash_ptrF =value++;
  
  FCTL1 = FWKEY;                            // Clear WRT bit
  FCTL3 = FWKEY+LOCK;
    FCTL1 = FWKEY+BLKWRT;
  *Flash_ptrG =value++;
  
  FCTL1 = FWKEY;                            // Clear WRT bit
  FCTL3 = FWKEY+LOCK;
    FCTL1 = FWKEY+BLKWRT;
  *Flash_ptrH =value++;
  
  FCTL1 = FWKEY;                            // Clear WRT bit
  FCTL3 = FWKEY+LOCK;
    FCTL1 = FWKEY+BLKWRT;
  *Flash_ptrI =value++;
  
  FCTL1 = FWKEY;                            // Clear WRT bit
  FCTL3 = FWKEY+LOCK;
    FCTL1 = FWKEY+BLKWRT;
  *Flash_ptrJ =value++;
  
  FCTL1 = FWKEY;                            // Clear WRT bit
  FCTL3 = FWKEY+LOCK;  
 // __bis_SR_register( GIE);
   halLcdPrintLine("done", 7, OVERWRITE_TEXT);
}
void socpage()
{
	  unsigned long * Flash_ptrD;               // Initialize Flash pointer Seg D
  unsigned long * Flash_ptrE;
  unsigned long * Flash_ptrF;
  unsigned long * Flash_ptrG;
  unsigned long * Flash_ptrH;
  unsigned long * Flash_ptrI;
  unsigned long * Flash_ptrJ;
	 Flash_ptrD = (unsigned long *) 0x1800;    // Initialize Flash pointer
   Flash_ptrE = (unsigned long *) 0x2000; 
    Flash_ptrF = (unsigned long *) 0x2200; 
     Flash_ptrG = (unsigned long *) 0x2400; 
      Flash_ptrH = (unsigned long *) 0x2600; 
       Flash_ptrI = (unsigned long *) 0x2800; 
        Flash_ptrJ = (unsigned long *) 0x3800;
	halLcdClearScreen();
  halLcdPrintLine("socpage", 5, OVERWRITE_TEXT);
  conv(*Flash_ptrD);
  halLcdPrintLine(val, 4, OVERWRITE_TEXT); 
    conv(*Flash_ptrE);
  halLcdPrintLine(val, 5, OVERWRITE_TEXT); 
    conv(*Flash_ptrF);
  halLcdPrintLine(val, 6, OVERWRITE_TEXT); 
    conv(*Flash_ptrG);
  halLcdPrintLine(val, 7, OVERWRITE_TEXT); 
    conv(*Flash_ptrH);
  halLcdPrintLine(val, 8, OVERWRITE_TEXT); 
    conv(*Flash_ptrI);
  halLcdPrintLine(val, 2, OVERWRITE_TEXT); 
  conv(*Flash_ptrJ);
  halLcdPrintLine(val, 1, OVERWRITE_TEXT);
  flag1=0;  flag1=flag2=0;
  
}
void batterypage()
{
	halLcdClearScreen();
	halLcdPrintLine("battery page", 5, OVERWRITE_TEXT);
	flag2=0;  flag1=flag2=0;
}
void batterypercentpage()
{
	halLcdClearScreen();
	halLcdPrintLine("battery percent page", 5, OVERWRITE_TEXT);
	flag2=0;  flag1=flag2=0;
}

int main()
{
	WDTCTL = WDTPW + WDTHOLD;                 // Stop WDT
          halLcdInit();
   halLcdBackLightInit();
    halLcdSetBackLight(0xFF);
    
    count=1;
    halLcdClearScreen();
  initi();
  menu_initi(count);
  freq_setup();
  prev=count;
  
 

  
  
   
       __bis_SR_register( GIE);
       while(1)
       {
       	if(flag!=0)
       	{
       	if ( prev!=count)
       	{
       	menu_initi(count);
       	prev=count;}
       	}else
       	{
       		if(count==1&&flag0!=0)
       		clockpage();
       		if(count==2&&flag1!=0)
       		socpage();
       		if(count==3&&flag2!=0)
       		batterypage();
       			
       	
       		}
      
       	
       
       };
       return 0;
}
#pragma vector=PORT2_VECTOR
__interrupt void Port2_ISR(void)
{
	   
switch(P2IFG)
{
case 0x08://select
    flag=0;
TA1R=0x00;
 	
     break;
case 0x10://up
    P1OUT|=0x03;
    if(count<=1)
    {
    count=3;
    
    }
    else
    { 
    	
   count--;
    }
      	
     break;
case 0x20://down
    if(count>=3)
    {
    count=1;
 
    }
    else 
    {
    count++;
    
    }
    break;
case 0x40://s1
     P1OUT ^=0x02;
     flag=1;
     flag0=flag1=flag2=1;
     count=1;
     prev=0;
     break;
case 0x80://s2
     P1OUT^=0x01;
     break;

}
TA1R=0x00;
    P2IFG=0;
  TA1CTL&=~TAIFG;
   
}

 #pragma vector=TIMER1_A0_VECTOR
__interrupt void TIMER1_A0_ISR(void)
{
	TA1CTL&=~TAIFG;
	TA1R=0x00;
	
  batterypercentpage();                           // Toggle P1.0
}   
    
	