#include "msp430.h"

void main(void)
{
int i,vold,vnew,pold,pnew,flag=0;
WDTCTL = WDTPW + WDTHOLD;  // Stop WDT
BCSCTL1 = CALBC1_1MHZ; // Set range
DCOCTL = CALDCO_1MHZ; // SMCLK = DCO = 1MHz
 P1DIR&=~0x03; 
 P1DIR |= 0x04;             // P1.6 to output
 P1SEL |= 0x04;             // P1.6 to TA0.1
 
TACTL = TASSEL_2 + MC_1;   
 CCR0 = 500;          		// PWM Period
 CCTL1 = OUTMOD_7;       	// CCR1 reset/set
while(1)
{  
	ADC10CTL0 = SREF_0 + ADC10SHT_2 + ADC10ON;
            		// P1.5 input, use ADC10CLK div 1, single channel mode  
    ADC10CTL1 =  INCH_4 + SHS_0 + ADC10SSEL_0 + ADC10DIV_0 + CONSEQ_0;
    ADC10AE0 = BIT4;      	// Enable ADC input on P1.1
    ADC10CTL0 |= ENC;     	// Enable conversions.
    ADC10CTL0 |= ADC10SC;  		 	// start a new conversion
    while ((ADC10CTL1 & ADC10BUSY) == 0x01);   	// wait for conversion to end
    vnew=ADC10MEM;      

ADC10CTL0 = SREF_0 + ADC10SHT_2 + ADC10ON;
            		// P1.5 input, use ADC10CLK div 1, single channel mode  
    ADC10CTL1 =  INCH_5 + SHS_0 + ADC10SSEL_0 + ADC10DIV_0 + CONSEQ_0;
    ADC10AE0 = BIT5;      	// Enable ADC input on P1.1
    ADC10CTL0 |= ENC;     	// Enable conversions.
    ADC10CTL0 |= ADC10SC;  		 	// start a new conversion
    while ((ADC10CTL1 & ADC10BUSY) == 0x01);   	// wait for conversion to end
    i=ADC10MEM;      
pnew=vnew*i;
if (flag==0) {pnew=pold;vold=vnew; flag=1;}
if(pnew>=pold)
{
	if(vnew>=vold) {CCR1=CCR1+5; _delay_cycles(100000);}
	else if(vnew<vold) {CCR1=CCR1-5; _delay_cycles(100000);}
}
else if(pnew<pold)
{
	if(vnew>=vold) {CCR1=CCR1-5; _delay_cycles(100000);}
	else if(vnew<vold) {CCR1=CCR1+5; _delay_cycles(100000);}
}
vold=vnew;
pold=pnew;
} 
}
