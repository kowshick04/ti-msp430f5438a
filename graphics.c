void display_percentage(int x,int y)
{
   halLcdLine(x-4,y+5,x+4,y-5,PIXEL_DARK);
   halLcdCircle(x+2,y+2,1,PIXEL_DARK);
   halLcdCircle(x-2,y-2,1,PIXEL_DARK);
}	
void display_bolt(int x,int y)
{
	halLcdLine(x,y,x+15,y-10,PIXEL_OFF);
	halLcdLine(x,y,x+10,y,PIXEL_OFF);
	halLcdLine(x+12,y-3,x+22,y-3,PIXEL_OFF);
	halLcdLine(x+22,y-3,x+5,y+10,PIXEL_OFF);
	halLcdLine(x+5,y+10,x+10,y,PIXEL_OFF);
	halLcdLine(x+15,y-10,x+12,y-3,PIXEL_OFF);
}
void display_battery()
{
   halLcdVLine(28,33,66,PIXEL_DARK);
   halLcdVLine(115,33,66,PIXEL_DARK);
   halLcdHLine(28,115,33,PIXEL_DARK);
   halLcdHLine(28,115,66,PIXEL_DARK);
   halLcdHLine(115,120,54,PIXEL_DARK);
   halLcdHLine(115,120,44,PIXEL_DARK);
   halLcdVLine(120,44,54,PIXEL_DARK);
   halLcdImage(BATTERY, 5, 24, 38, 38);
}		
