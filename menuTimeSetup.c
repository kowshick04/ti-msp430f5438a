#include "msp430x54x.h"
#include "hal_lcd.h"
#include "string.h"


#define HOUR10    (0)
#define HOUR01    (1)
#define COLON2    (2)
#define MINUTE10  (3)
#define MINUTE01  (4)
#define COLON1    (5)
#define SECOND10  (6)
#define SECOND01  (7)
char time[9] = {'0', '4', ':', '3', '0', ':', '0', '0', '\0'};
char val[]="XXXX\0";
int secFlag=0,hourFlag=1,minFlag=0,rtcFlag=1,onTime=0;
  int count=1;
 int flag=1,flag0=1,flag1=1,flag2=1;
  int prev=0,page=0;
  char *k,*l,*m;
  int CLOCK_FLAG=0;
  int conv(int adc_val)
{    unsigned char thousands = '0'; 
	 unsigned char hundreds = '0';           // Init to '0' to code in ASCII
    unsigned char tens = '0';
    unsigned char ones = '0';
   while (adc_val >= 1000){                 // Calculate hundreds' place
       thousands++;
        adc_val -= 1000;
    }
    while (adc_val >= 100){                 // Calculate hundreds' place
        hundreds++;
        adc_val -= 100;
    }
  // if (hundreds == '0') hundreds = ' ';    //eliminate preceding zero, if any
    while (adc_val >= 10){                  // Calculate tens' place
        tens++;
        adc_val -= 10;
    }
    ones += adc_val; 
    val[3]= ones;
    val[2]=tens;
    val[1]=hundreds;
    val[0]=thousands;
    return 0;
}
  int GetRTCHOUR(void)
{
	
    return RTCHOUR;
}


int GetRTCMIN(void)
{
	//RTCMIN=0;
    return RTCMIN;
}
int GetRTCSEC(void)
{
	//RTCSEC=0;
    return RTCSEC;
}
int SetRTCYEAR(int year)
{
    RTCYEAR = year;
    return year;
}

int SetRTCMON(int month)
{
    RTCMON = month;
    return month;
}

int SetRTCDAY(int day)
{
    RTCDAY = day;
    return day;
}

int SetRTCDOW(int dow)
{
    RTCDOW = dow;
    return dow;
}

int SetRTCHOUR(int hour)
{
    RTCHOUR = hour;
    return hour;
}

int SetRTCMIN(int min)
{
    RTCMIN = min;
    return min;
}

int SetRTCSEC(int sec)
{
    RTCSEC = sec;
    return sec;
}
/**********************************************************************//**
 * @brief  Adds one to the current hour when setting the time.
 *
 * @param  none
 *
 * @return none
 *************************************************************************/

static void hourAdd(void)
{
    unsigned int hourBCD = GetRTCHOUR();

    if ((hourBCD & 0x0F) == 9)
    {
        hourBCD &= 0xF0;                    // low digit = 0
        hourBCD += 0x10;                    // Add 1 to high digit
        SetRTCHOUR(hourBCD);
    }
    else if ((hourBCD & 0x0F) == 0x03 && (hourBCD & 0xF0) == 0x20)
    {
        hourBCD = 0;
        SetRTCHOUR(hourBCD);
    }
    else
    {
        hourBCD++;
        SetRTCHOUR(hourBCD);
    }
}

/**********************************************************************//**
 * @brief  Adds one to the current minute when setting the time.
 *
 * @param  none
 *
 * @return none
 *************************************************************************/

static void minuteAdd(void)
{
    unsigned int minuteBCD = GetRTCMIN();

    if ((minuteBCD & 0x0F) == 9)
    {
        minuteBCD &= 0xF0;                  // Low digit = 0
        if ((minuteBCD & 0xF0) == 0x50)     // If high digit = 5
            SetRTCMIN(0x00);                // Reset to 0
        else
        {
            minuteBCD += 0x10;              // Add 1 to high digit
            SetRTCMIN(minuteBCD);
        }
    }
    else
    {
        minuteBCD++;
        SetRTCMIN(minuteBCD);
    }
}

/**********************************************************************//**
 * @brief  Adds one to the current second when setting the time.
 *
 * @param  none
 *
 * @return none
 *************************************************************************/

static void secondAdd(void)
{
    unsigned int secondBCD = GetRTCSEC();

    RTCCTL01 |= RTCHOLD;

    if ((secondBCD & 0x0F) == 9)
    {
        secondBCD &= 0xF0;                  // low digit = 0
        if ((secondBCD & 0xF0) == 0x50)     // if high digit = 5
        {
            SetRTCSEC(0x00);                // reset to 0
        }
        else
        {
            secondBCD += 0x10;
            SetRTCSEC(secondBCD);           // add 1 to high digit
        }
    }
    else
    {
        secondBCD++;
        SetRTCSEC(secondBCD);
    }

    RTCCTL01 &= ~RTCHOLD;
}

/**********************************************************************//**
 * @brief  Subtracts one from the current hour when setting the time.
 *
 * @param  none
 *
 * @return none
 *************************************************************************/

static void hourSub(void)
{
    unsigned int hourBCD = GetRTCHOUR();

    if ((hourBCD & 0x0F) == 0x00)           // Is least-significant digit = 0?
    {
        if ((hourBCD & 0xF0) == 0x00)
        {
            hourBCD = 0x23;
            SetRTCHOUR(hourBCD);
        }
        else
        {
            hourBCD |= 0x09;                // Set lowest digit to '9'
            hourBCD -= 0x10;                // Subtract one from the lowest digit
            SetRTCHOUR(hourBCD);
        }
    }
    else
    {
        hourBCD--;
        SetRTCHOUR(hourBCD);
    }
}

/**********************************************************************//**
 * @brief  Subtracts one from the current minute when setting the time.
 *
 * @param  none
 *
 * @return none
 *************************************************************************/

static void minuteSub(void)
{
    unsigned int minuteBCD = GetRTCMIN();

    if ((minuteBCD & 0x0F) == 0x00)
    {
        if (minuteBCD == 0)
            SetRTCMIN(0x59);
        else
        {
            minuteBCD |= 0x09;                   //low digit = 9
            minuteBCD -= 0x10;
            SetRTCMIN(minuteBCD);
        }
    }
    else
    {
        minuteBCD--;
        SetRTCMIN(minuteBCD);
    }
}

/**********************************************************************//**
 * @brief  Subtracts one from the current second when setting the time.
 *
 * @param  none
 *
 * @return none
 *************************************************************************/

static void secondSub(void)
{
    unsigned int secondBCD = GetRTCSEC();

    RTCCTL01 |= RTCHOLD;

    if ((secondBCD & 0x0F) == 0x00)
    {
        if (secondBCD == 0)
            SetRTCSEC(0x59);
        else
        {
            secondBCD |= 0x09;              //low digit = 9
            secondBCD -= 0x10;
            SetRTCSEC(secondBCD);
        }
    }
    else
    {
        secondBCD--;
        SetRTCSEC(secondBCD);
    }

    RTCCTL01 &= ~RTCHOLD;
}

/**********************************************************************//**
 * @brief  Updates the hours, minutes and seconds on the digital clock
 *
 * @param  none
 *
 * @return none
 *************************************************************************/

void updateDigitalClock(void)
{
    unsigned int hourBCD   = GetRTCHOUR();
    unsigned int minuteBCD = GetRTCMIN();
    unsigned int secondBCD = GetRTCSEC();

    time[HOUR10]   = '0' + (hourBCD   >> 4);
    time[HOUR01]   = '0' + (hourBCD   &  0x0F);
    time[MINUTE10] = '0' + (minuteBCD >> 4);
    time[MINUTE01] = '0' + (minuteBCD &  0x0F);
    time[SECOND10] = '0' + (secondBCD >> 4);
    time[SECOND01] = '0' + (secondBCD &  0x0F);
}



void setupRTC(void)
{
    RTCCTL01 = RTCMODE + RTCBCD + RTCHOLD + RTCTEV_1;
   if(rtcFlag==1)
   {
   	
    SetRTCHOUR(0x03);
    SetRTCMIN(0x49);
    SetRTCSEC(0x56);
    SetRTCDAY(0x01);
    SetRTCMON(0x01);
    SetRTCYEAR(0x2012);
    rtcFlag=0;
   }

    RTCCTL01 &= ~RTCHOLD;

    RTCPS1CTL = RT1IP_5;                    // Interrupt freq: 2Hz
    RTCPS0CTL = RT0IP_7; 
   
    	
    RTCAHOUR=3;
    RTCAMIN=50;
    
   


  // AE=1;
  //RTCCTL0 |=  RTCTEVIE+RTCAIE;         // Enable interrupt
}
void SetVcoreUp (unsigned int level)
{
  // Open PMM registers for write
  PMMCTL0_H = PMMPW_H;              
  // Set SVS/SVM high side new level
  SVSMHCTL = SVSHE + SVSHRVL0 * level + SVMHE + SVSMHRRL0 * level;
  // Set SVM low side to new level
  SVSMLCTL = SVSLE + SVMLE + SVSMLRRL0 * level;
  // Wait till SVM is settled
  while ((PMMIFG & SVSMLDLYIFG) == 0);
  // Clear already set flags
  PMMIFG &= ~(SVMLVLRIFG + SVMLIFG);
  // Set VCore to new level
  PMMCTL0_L = PMMCOREV0 * level;
  // Wait till new level reached
  if ((PMMIFG & SVMLIFG))
    while ((PMMIFG & SVMLVLRIFG) == 0);
  // Set SVS/SVM low side to new level
  SVSMLCTL = SVSLE + SVSLRVL0 * level + SVMLE + SVSMLRRL0 * level;
  // Lock PMM registers for write access
  PMMCTL0_H = 0x00;
}
void menu_initi(int a)
{
	    halLcdClearScreen();
halLcdPrintLine("   BMS:    ", 3, OVERWRITE_TEXT);
halLcdPrintLine("1.Clock", 4, OVERWRITE_TEXT);
halLcdPrintLine("2.SOC", 5,  OVERWRITE_TEXT);
halLcdPrintLine("3.Battery", 6, OVERWRITE_TEXT);

switch(a)
{
	case 1:
	halLcdPrintLine("1.Clock", 4, INVERT_TEXT|OVERWRITE_TEXT);
	break;
	case 2:
	halLcdPrintLine("2.SOC", 5, INVERT_TEXT|OVERWRITE_TEXT);
	break;
	case 3:
	halLcdPrintLine("3.Battery", 6, INVERT_TEXT|OVERWRITE_TEXT);
	break;
}
 
}
void initi()
{
	P2DIR=0x00;
	
	P2REN|=0xFF;
	P2OUT|=0xFF;
	P2IE|=0xFF;
	P2IES|=0xFF;
	
	P1DIR|=0x03;
	P1OUT|=0x03;
//	TA1CCTL0 = CCIE;                          // CCR0 interrupt enabled
//  TA1CCR0 = 65535;
//  TA1CTL = TASSEL_1 + MC_1 + TACLR+ID_3;         // SMCLK, upmode, clear TAR
	
}
void freq_setup()
{
	 SetVcoreUp (0x01);
 SetVcoreUp (0x02);  
 SetVcoreUp (0x03);
	UCSCTL3 = SELREF_2;                       // Set DCO FLL reference = REFO
  UCSCTL4 |= SELA_2;  
  UCSCTL0 = 0x0000;                         // Set lowest possible DCOx, MODx
  UCSCTL1 = DCORSEL_5;                      // Select DCO range 50MHz operation
  UCSCTL2 = FLLD_1 + 762;                       // Set ACLK = REFO
   do
  {
    UCSCTL7 &= ~(XT2OFFG + XT1LFOFFG + XT1HFOFFG + DCOFFG);
                                            // Clear XT2,XT1,DCO fault flags
    SFRIFG1 &= ~OFIFG;                      // Clear fault flags
  }while (SFRIFG1&OFIFG);   
}
void clockpage()
{
	halLcdClearScreen();

  
  CLOCK_FLAG=1;
  RTCCTL01 |= RTCHOLD;
  if(hourFlag==1)
  {
  halLcdPrintLineCol(&time[HOUR10], 4,3, INVERT_TEXT|OVERWRITE_TEXT);
  halLcdPrintLineCol(&time[COLON2], 4,5, OVERWRITE_TEXT);
  }
  if(minFlag==1)
  {
  	 halLcdPrintLineCol(&time[HOUR10], 4,3, OVERWRITE_TEXT);
  	halLcdPrintLineCol(&time[MINUTE10], 4,6, INVERT_TEXT|OVERWRITE_TEXT);
  halLcdPrintLineCol(&time[COLON1], 4,8, OVERWRITE_TEXT);
  }
    if(secFlag==1)
  {
  	
  	
  	 halLcdPrintLineCol(&time[HOUR10], 4,3, OVERWRITE_TEXT);
  	halLcdPrintLineCol(&time[SECOND10], 4,9, INVERT_TEXT|OVERWRITE_TEXT);
    }
    
  
    
  flag0=0;
  flag1=flag2=0;
}
void socpage()
{
	halLcdClearScreen();
  halLcdPrintLine("socpage", 5, OVERWRITE_TEXT);
  flag1=0;  flag1=flag2=0;
}
void batterypage()
{
	halLcdClearScreen();
	halLcdPrintLine("battery page", 5, OVERWRITE_TEXT);
	flag2=0;  flag1=flag2=0;
}
void batterypercentpage()
{
	halLcdClearScreen();
	halLcdPrintLine("battery percent page", 5, OVERWRITE_TEXT);
	flag2=0;  flag1=flag2=0;
}
void main()
{
	
	WDTCTL = WDTPW + WDTHOLD;                 // Stop WDT
	freq_setup();
          halLcdInit();
   halLcdBackLightInit();
    halLcdSetBackLight(0xFF);
    initi();
    
    setupRTC();
     halLcdClearScreen();
    menu_initi(count);
   
   __bis_SR_register( GIE);
    while(1)
    {
    updateDigitalClock();	
  
	halLcdPrintLineCol(&time[HOUR10], 8, 0,  OVERWRITE_TEXT);
	if(flag!=0)
       	{
       	if ( prev!=count)
       	{
       	menu_initi(count);
       	prev=count;}
       	}
    else
       	{
       		if(count==1&&flag0!=0)
       		clockpage();
       		if(count==2&&flag1!=0)
       		socpage();
       		if(count==3&&flag2!=0)
       		batterypage();
       			
       	
       		}
    }
    
}

#pragma vector=PORT2_VECTOR
__interrupt void Port2_ISR(void)
{
	   
switch(P2IFG)
{
case 0x08://select
    flag=0;
//TA1R=0x00;
 	
     break;
case 0x10://up
    P1OUT|=0x03;
      if(  CLOCK_FLAG==1)
      {
      	if(hourFlag==1)
      	hourAdd();
        if(secFlag==1)
        secondAdd();
        if(minFlag==1)
        minuteAdd();
        
      	updateDigitalClock();
      	clockpage();
      	
      } 
      if(flag!=0)
      { 	
    if(count<=1)
    count=3;
    else
   count--;
    
}	
     break;
case 0x20://down
  
  	if(  CLOCK_FLAG==1)
      {
      	if(hourFlag==1)
      	hourSub();
        if(secFlag==1)
        secondSub();
        if(minFlag==1)
        minuteSub();
        
      	updateDigitalClock();
      	clockpage();
      	
      }
 if(flag!=0)
  {
    if(count>=3)
    count=1;
    else 
    count++;
  }
 break;
case 0x40://s1
     P1OUT ^=0x02;
     flag=1;
     flag0=flag1=flag2=1;
     count=1;
     RTCCTL01 &= ~RTCHOLD;
     prev=0;
     CLOCK_FLAG=0;
     
     
     break;
case 0x80://s2
     P1OUT^=0x01;
     break;
case 0x02: //left
  
if(hourFlag==1)
{
	secFlag=1;
	minFlag=0;
	hourFlag=0;
	
}
else if(secFlag==1)
{
	secFlag=0;
	minFlag=1;
	hourFlag=0;
}
else if(minFlag==1)
{
	secFlag=0;
	minFlag=0;
	hourFlag=1;
	
}

clockpage();	
break;
case 0x04: //right
 
if(hourFlag==1)
{
	secFlag=0;
	minFlag=1;
	hourFlag=0;
	
}else if(minFlag==1)
{
	secFlag=1;
	minFlag=0;
	hourFlag=0;
	
}
else if(secFlag==1)
{
	secFlag=0;
	minFlag=0;
	hourFlag=1;
}
clockpage();


break;
}
//TA1R=0x00;
    P2IFG=0;
  }

 
  

	

