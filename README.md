Embedded system implementation of Smart Inverter system (Award winning project at Texas Instruments India Innovation Challenge 2013, won a prize money of $5000)

* Complete embedded code for TI MSP430F5438A microcontroller to implement the proposed system.
* Demo [URL](https://www.youtube.com/watch?v=GEOWGyvjKy0) of the project.
