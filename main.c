/********************************************************************
 * Pin connections 
 * P5.0 -Battery relay pin
 * P5.1 -Grid relay pin
 * P6.7 -voltage
 * P7.4 -battery current 
 * P7.5 - Panel current
 * Don't forget to intialize prev_state variable properly, which is the state of grid relay based on SoC 
 * Note : Connect everything properly and press start, only after that 
 * Soc is measured through open circuit voltage method and coulomb counting begins...otherwise nothing will work
 * Recalibration is disabled for now. Check the rest */
#include "msp430F5438A.h"
#include "hal_lcd.h"
#include "string.h"
#include "UserExperienceGraphics.h"
#include "hal_usb.h"
#include "hal_board.h"
#define HOUR10    (0)
#define HOUR01    (1)
#define COLON2    (2)
#define MINUTE10  (3)
#define MINUTE01  (4)
#define COLON1    (5)
#define SECOND10  (6)
#define SECOND01  (7)

char time[9] = {'0', '4', ':', '3', '0', ':', '0', '0', '\0'};
char val[]="XX.XX\0", temp_cat[6],val2[]="XXXX\0";
int secFlag=0,hourFlag=1,minFlag=0,current=0,percentage=0,voltage=0,OCV_FLAG=0,solar_current=0,solar_voltage=0;
int prev_state=0;//0 ON
int min1_flag=0;
long double  adc_value=0,i=0,coloumb_count=0;
int count=1,counter=0,coulomb=0;
int powerCutFlag=1,powerOnFlag=0;
int flag=1,flag0=1,flag1=1,flag2=1,flag3=1,flag4=1, MENU_FLAG=1;
int temp_minute=0, temp_minute2=0, temp_sec=0,temp_hour=0,hour=0;
long double slope=0,voc=0,soc=0,power=0;
double adc_1min=0,adc_2min=0;
int prev=0,page=0;
int temp=1;
int upCheck=0;
int ALARM_MINUTE=0, ALARM_HOUR=0,ALARM_MINUTE1=0,ALARM_MINUTE5=0;
int CLOCK_FLAG=0,boxFlag=1,chargeFlag=0,dischargeFlag=0,currentFlag=0;

int conv(int adc_val)
{    unsigned char thousands = '0'; 
	 unsigned char hundreds = '0';           // Init to '0' to code in ASCII
	 unsigned char tens = '0';
     unsigned char ones = '0';
     while (adc_val >= 1000){                 // Calculate hundreds' place
       thousands++;
        adc_val -= 1000;
    }
    while (adc_val >= 100){                 // Calculate hundreds' place
        hundreds++;
        adc_val -= 100;
    }
  // if (hundreds == '0') hundreds = ' ';    //eliminate preceding zero, if any
    while (adc_val >= 10){                  // Calculate tens' place
        tens++;
        adc_val -= 10;
    }
    ones += adc_val; 
    val[4]= ones;
    val[3]=tens;
    val[1]=hundreds;
    val[0]=thousands;
    return 0;
}
  int convp(int adc_val)
{    unsigned char thousands = '0'; 
	 unsigned char hundreds = '0';           // Init to '0' to code in ASCII
     unsigned char tens = '0';
     unsigned char ones = '0';
     while (adc_val >= 1000){                 // Calculate hundreds' place
	 thousands++;
	 adc_val -= 1000;
    }
    while (adc_val >= 100){                 // Calculate hundreds' place
	 hundreds++;
	 adc_val -= 100;
    }
  // if (hundreds == '0') hundreds = ' ';    //eliminate preceding zero, if any
    while (adc_val >= 10){                  // Calculate tens' place
	 tens++;
	 adc_val -= 10;
    }
    ones += adc_val; 
    val2[3]= ones;
    val2[2]=tens;
    val2[1]=hundreds;
    val2[0]=thousands;
    return 0;
}


  int GetRTCHOUR(void)
{
	//RTCHOUR=0;
    return RTCHOUR;
}


int GetRTCMIN(void)
{
	//RTCMIN=0;
    return RTCMIN;
}
int GetRTCSEC(void)
{
	//RTCSEC=0;
    return RTCSEC;
}
int SetRTCYEAR(int year)
{
    RTCYEAR = year;
    return year;
}

int SetRTCMON(int month)
{
    RTCMON = month;
    return month;
}

int SetRTCDAY(int day)
{
    RTCDAY = day;
    return day;
}

int SetRTCDOW(int dow)
{
    RTCDOW = dow;
    return dow;
}

int SetRTCHOUR(int hour)
{
    RTCHOUR = hour;
    return hour;
}

int SetRTCMIN(int min)
{
    RTCMIN = min;
    return min;
}

int SetRTCSEC(int sec)
{
    RTCSEC = sec;
    return sec;
}
/**********************************************************************//**
 * @brief  Adds one to the current hour when setting the time.
 *
 * @param  none
 *
 * @return none
 *************************************************************************/

static void hourAdd(void)
{
    unsigned int hourBCD = GetRTCHOUR();

    if ((hourBCD & 0x0F) == 9)
    {
        hourBCD &= 0xF0;                    // low digit = 0
        hourBCD += 0x10;                    // Add 1 to high digit
        SetRTCHOUR(hourBCD);
    }
    else if ((hourBCD & 0x0F) == 0x03 && (hourBCD & 0xF0) == 0x20)
    {
        hourBCD = 0;
        SetRTCHOUR(hourBCD);
    }
    else
    {
        hourBCD++;
        SetRTCHOUR(hourBCD);
    }
}

/**********************************************************************//**
 * @brief  Adds one to the current minute when setting the time.
 *
 * @param  none
 *
 * @return none
 *************************************************************************/

static void minuteAdd(void)
{
    unsigned int minuteBCD = GetRTCMIN();

    if ((minuteBCD & 0x0F) == 9)
    {
        minuteBCD &= 0xF0;                  // Low digit = 0
        if ((minuteBCD & 0xF0) == 0x50)     // If high digit = 5
            SetRTCMIN(0x00);                // Reset to 0
        else
        {
            minuteBCD += 0x10;              // Add 1 to high digit
            SetRTCMIN(minuteBCD);
        }
    }
    else
    {
        minuteBCD++;
        SetRTCMIN(minuteBCD);
    }
}

/**********************************************************************//**
 * @brief  Adds one to the current second when setting the time.
 *
 * @param  none
 *
 * @return none
 *************************************************************************/

static void secondAdd(void)
{
    unsigned int secondBCD = GetRTCSEC();

    RTCCTL01 |= RTCHOLD;
   if ((secondBCD & 0x0F) == 9)
    {
        secondBCD &= 0xF0;                  // low digit = 0
        if ((secondBCD & 0xF0) == 0x50)     // if high digit = 5
        {
            SetRTCSEC(0x00);                // reset to 0
        }
        else
        {
            secondBCD += 0x10;
            SetRTCSEC(secondBCD);           // add 1 to high digit
        }
    }
    else
    {
        secondBCD++;
        SetRTCSEC(secondBCD);
    }

    RTCCTL01 &= ~RTCHOLD;
}
/**********************************************************************//**
 * @brief  Subtracts one from the current hour when setting the time.
 *
 * @param  none
 *
 * @return none
 *************************************************************************/
static void hourSub(void)
{
    unsigned int hourBCD = GetRTCHOUR();

    if ((hourBCD & 0x0F) == 0x00)           // Is least-significant digit = 0?
    {
        if ((hourBCD & 0xF0) == 0x00)
        {
            hourBCD = 0x23;
            SetRTCHOUR(hourBCD);
        }
        else
        {
            hourBCD |= 0x09;                // Set lowest digit to '9'
            hourBCD -= 0x10;                // Subtract one from the lowest digit
            SetRTCHOUR(hourBCD);
        }
    }
    else
    {
        hourBCD--;
        SetRTCHOUR(hourBCD);
    }
}

/**********************************************************************//**
 * @brief  Subtracts one from the current minute when setting the time.
 *
 * @param  none
 *
 * @return none
 *************************************************************************/

static void minuteSub(void)
{
    unsigned int minuteBCD = GetRTCMIN();

    if ((minuteBCD & 0x0F) == 0x00)
    {
        if (minuteBCD == 0)
            SetRTCMIN(0x59);
        else
        {
            minuteBCD |= 0x09;                   //low digit = 9
            minuteBCD -= 0x10;
            SetRTCMIN(minuteBCD);
        }
    }
    else
    {
        minuteBCD--;
        SetRTCMIN(minuteBCD);
    }
}

/**********************************************************************//**
 * @brief  Subtracts one from the current second when setting the time.
 *
 * @param  none
 *
 * @return none
 *************************************************************************/

static void secondSub(void)
{
    unsigned int secondBCD = GetRTCSEC();

    RTCCTL01 |= RTCHOLD;

    if ((secondBCD & 0x0F) == 0x00)
    {
        if (secondBCD == 0)
            SetRTCSEC(0x59);
        else
        {
            secondBCD |= 0x09;              //low digit = 9
            secondBCD -= 0x10;
            SetRTCSEC(secondBCD);
        }
    }
    else
    {
        secondBCD--;
        SetRTCSEC(secondBCD);
    }

    RTCCTL01 &= ~RTCHOLD;
}

/**********************************************************************//**
 * @brief  Updates the hours, minutes and seconds on the digital clock
 *
 * @param  none
 *
 * @return none
 *************************************************************************/
void updateDigitalClock(void)
{
    unsigned int hourBCD   = GetRTCHOUR();
    unsigned int minuteBCD = GetRTCMIN();
    unsigned int secondBCD = GetRTCSEC();
    time[HOUR10]   = '0' + (hourBCD   >> 4);
    time[HOUR01]   = '0' + (hourBCD   &  0x0F);
    time[MINUTE10] = '0' + (minuteBCD >> 4);
    time[MINUTE01] = '0' + (minuteBCD &  0x0F);
    time[SECOND10] = '0' + (secondBCD >> 4);
    time[SECOND01] = '0' + (secondBCD &  0x0F);
}
void adc_setup()
{
 P6SEL = 0x80;  
 P7SEL=0x70;
   ADC12CTL0 = ADC12ON+ADC12MSC+ADC12SHT0_2; // Turn on ADC12, set sampling time
  ADC12CTL1 = ADC12SHP+ADC12CONSEQ_3;       // Use sampling timer, single sequence
  ADC12MCTL0 = ADC12SREF_0+ADC12INCH_7;                 // ref+=AVcc, channel = A0
  ADC12MCTL1 =ADC12SREF_0+ ADC12INCH_12;                 // ref+=AVcc, channel = A1
  ADC12MCTL2 =ADC12SREF_0+ ADC12INCH_13;                 // ref+=AVcc, channel = A2
  ADC12MCTL3 = ADC12SREF_0+ADC12INCH_14;        // ref+=AVcc, channel = A3, end seq.          // Turn on ADC12, set sampling time
    
  ADC12CTL2 = ADC12RES_2;                   // Use sampling timer
                   
  ADC12CTL0 |= ADC12ENC;                    // Enable conversions
                            
}
void display_percentage(int x,int y)
{
   halLcdLine(x-4,y+5,x+4,y-5,PIXEL_DARK);
   halLcdCircle(x+2,y+2,1,PIXEL_DARK);
   halLcdCircle(x-2,y-2,1,PIXEL_DARK);
}	
void display_bolt(int x,int y)
{
	halLcdLine(x,y,x+15,y-10,PIXEL_OFF);
	halLcdLine(x,y,x+10,y,PIXEL_OFF);
	halLcdLine(x+12,y-3,x+22,y-3,PIXEL_OFF);
	halLcdLine(x+22,y-3,x+5,y+10,PIXEL_OFF);
	halLcdLine(x+5,y+10,x+10,y,PIXEL_OFF);
	halLcdLine(x+15,y-10,x+12,y-3,PIXEL_OFF);
}

void add_min(int a)
{
	int i=0;
	temp_minute=RTCMIN;
	for(i=0;i<a;i++)
	{
		if((temp_minute&0x0F)==9)
		{
			if((temp_minute&0xF0)==0x50)
			{
				temp_minute=0x00;
				hour++;
			}	
			else
			{
				temp_minute&=0xF0;
				temp_minute+=0x10;
			}
		}
		else
		temp_minute++;
	}
}
timerb_setup()
{
  TBCCTL0 = CCIE;                           // CCR0 interrupt enabled
  TBCCR0 = 33000;
  TBCTL = TBSSEL_1 + MC_1 + TBCLR;         // SMCLK, contmode, clear TBR
	//halLcdPrintLine("initialised..", 1, OVERWRITE_TEXT);
}
void add_hour(int a)
{
	int i=0;
	temp_hour=RTCHOUR;
	for(i=0;i<a;i++)
	{
		if((temp_hour&0x0F)==3)
		{
			if((temp_hour&0xF0)==0x20)
			{
				temp_hour=0x00;
			}	
			else
			{
				temp_hour&=0xF0;
				temp_hour+=0x10;
			}
		}
		else
		temp_hour++;
	}	
}

void setupRTC(void)
{
    RTCCTL01 = RTCMODE + RTCBCD + RTCHOLD + RTCTEV_1+RTCAIE;
    SetRTCHOUR(0x09);
    SetRTCMIN(0x30);
    SetRTCSEC(0x05);
    SetRTCDAY(0x01);
    SetRTCMON(0x01);
    SetRTCYEAR(0x2012);
    RTCCTL01 &= ~RTCHOLD;
    RTCPS1CTL = RT1IP_7;                   
    RTCPS0CTL = RT0IP_7;                    
	RTCAHOUR=BIT7;
	RTCAMIN=BIT7;
	
////    RTCCTL0 |= RTCRDYIE + RTCTEVIE;         // Enable interrupt
}

void power_interrupt()
{
	halLcdClearScreen();
	P1DIR&=~0x10;     	//P1.4 input
	P1REN|=0x10;     	// Enabling Pull up or down
	if(!(P1IN&0x01))
	{
	halLcdPrintLine("Mains Off", 1, INVERT_TEXT|OVERWRITE_TEXT);	
	
    P1OUT&=~0x10;     	//Pull down P1,4
    P1IE|=0x10;       	//Enable external interrupt
    P1IES&=~0x10;      	//Rising edge
if(OCV_FLAG==1)
{
  TB0CTL|=TBIE;
  TB0CCTL0|=CCIE;
  TB0R=0x00;
  TB0CTL&=~TBIFG;
  halLcdActive();
  halLcdBackLightInit();
  halLcdSetBackLight(0xCF);
  halLcdSetContrast(100);
  OCV_FLAG=0;
}  
}
	else 
	{
	halLcdPrintLine("Mains on",1, INVERT_TEXT|OVERWRITE_TEXT);	
P1OUT|=0x10;     	//Pull up P1,4
P1IE|=0x10;       	//Enable external interrupt
P1IES|=0x10;      	//falling edge
	}
	
}

void intial_display()
{

	halLcdClearScreen();
	halLcdPrintLine("Initialising SoC",5, INVERT_TEXT|OVERWRITE_TEXT);
	_delay_cycles(20000000);
	halLcdClearScreen();
	halLcdPrintLine("Calibrating SoC",5, INVERT_TEXT|OVERWRITE_TEXT);
	_delay_cycles(20000000);
  for(i=0;i<1000;i++)
  {
	while(!(ADC12IFG&BIT0));
    counter=ADC12MEM0_L;
    counter=counter|ADC12MEM0_H<<8;
    adc_value+=counter;
  }
    adc_value=adc_value/1000;
	adc_value=adc_value*0.4520;	
    adc_value/=100;
    soc=50+78.125*(adc_value-12.20);
	soc*=10080;
	conv(adc_value*100);
    halLcdPrintLine("Voltage", 1, OVERWRITE_TEXT);
    halLcdPrintLineCol(val, 1,10, OVERWRITE_TEXT);
    halLcdPrintLineCol("V", 1,15, OVERWRITE_TEXT);
    //soc=483120;
    adc_value=0;
}	
/**************************************************************************
 * Initialises the system by connecting Battery and Grid to the inverter. *
 * Initalizing the coulomb counting process                               *
 * ************************************************************************/
void start()
{   
	intial_display();
	P5DIR|=0x01;    //Config Battery Relay pin 
	P5OUT|=0x01;    //Turn on  Battery Relay
	P5DIR|=0x02;   //Config Grid Relay pin
	P5OUT&=~0x02;  //On Grid Relay
	
	adc_value=0;
	MENU_FLAG=0;
	//halLcdClearScreen();
    ADC12CTL0 |= ADC12SC; 
//	while(!(ADC12IFG&BIT0));
//counter=ADC12MEM0_L;
// adc_value=counter|ADC12MEM0_H<<8; 
//    adc_value=adc_value*0.4520;  
//    conv(adc_value);
//    halLcdPrintLine("Voltage ", 1, OVERWRITE_TEXT);
//    halLcdPrintLineCol(val, 1,11, OVERWRITE_TEXT);
//    halLcdPrintLineCol("V", 1,15, OVERWRITE_TEXT);
 	
    halLcdPrintLine("Battery conneted", 4, OVERWRITE_TEXT);
    halLcdPrintLine("Grid connected",5,OVERWRITE_TEXT);
	timerb_setup();
    flag0=0; flag1=flag2=flag3=0;
    currentFlag=0;
}
/***********************************************************************
 * Funtion to implement 1 minute and 5 minute SoC recalibration process*
 * *********************************************************************/
  
  	 
void socpage()
{
//Initiating the Recalibration process
if(RTCAMIN==(BIT7|ALARM_MINUTE))
{
	int i;
	OCV_FLAG=1;
	TB0CTL&=~TBIE;
    TB0CCTL0&=~CCIE;
	//halLcdShutDownBackLight();
   // halLcdStandby();
    halLcdClearScreen();
   	halLcdPrintLine("Recalibrating", 3, OVERWRITE_TEXT);
   	for(i=13;i<=16;i++)
   	{
   	  halLcdPrintLineCol(".", 3,i, OVERWRITE_TEXT);	
   	  _delay_cycles(20000000);
   	}  
   		
	P10OUT|=0x01;
	P5OUT&=~0x01;
	adc_value=0;
    MENU_FLAG=0;
	halLcdClearScreen();
	conv(soc/100.8);
	RTCAHOUR=BIT7;
	RTCAMIN=BIT7;
	add_min(1);
	add_hour(hour);
	ALARM_MINUTE1=temp_minute;
	ALARM_HOUR=temp_hour;
	RTCAHOUR|=ALARM_HOUR;
	RTCAMIN|= ALARM_MINUTE1;
}
//Sampling Voltage at first minute
else if(RTCAMIN==(BIT7|ALARM_MINUTE1))
{
ADC12CTL0 |= ADC12SC; 
  for(i=0;i<1000;i++)
  {
 while(!(ADC12IFG&BIT0));
 counter=ADC12MEM0_L;
 counter=counter|ADC12MEM0_H<<8;
 adc_value+=counter;
  }
  halLcdClearScreen();
    adc_value=adc_value/1000;
	adc_value=adc_value*0.4520;		
	adc_1min=adc_value;
	conv(adc_1min);
	halLcdPrintLine("1 Min      :", 2, OVERWRITE_TEXT);
    halLcdPrintLineCol(val, 2,11, OVERWRITE_TEXT);
    halLcdPrintLineCol("V", 2,16, OVERWRITE_TEXT);
	adc_value=0;
	min1_flag=1;
	RTCAHOUR=BIT7;
	RTCAMIN=BIT7;
	add_min(4);
	add_hour(hour);
	ALARM_MINUTE5=temp_minute;
	ALARM_HOUR=temp_hour;
	RTCAHOUR|=ALARM_HOUR;
	RTCAMIN|= ALARM_MINUTE5;
}
//Sampling voltage at fifth minute
else if(RTCAMIN==(BIT7|ALARM_MINUTE5))
{	
  for(i=0;i<1000;i++)
  {
	while(!(ADC12IFG&BIT0));
    counter=ADC12MEM0_L;
    counter=counter|ADC12MEM0_H<<8;
	adc_value+=counter;
  }
//  	halLcdActive();
//	halLcdBackLightInit();
//    halLcdSetBackLight(0xCF);
//    halLcdSetContrast(100);
//    halLcdClearScreen();
  	adc_value=adc_value/1000;
	adc_value=adc_value*0.4520;
	adc_2min=adc_value;
    conv(adc_2min);
    halLcdPrintLine("5 Min      :", 3, OVERWRITE_TEXT);
    halLcdPrintLineCol(val, 3,11, OVERWRITE_TEXT);
    halLcdPrintLineCol("V", 3,16, OVERWRITE_TEXT);	
    slope=(adc_2min-adc_1min)/0.7;
	slope/=100;
	voc=(1.64*slope)+(adc_1min/100);
	conv(voc*100);
//Calculating the SoC 
	halLcdPrintLine("Predicted  ", 4, OVERWRITE_TEXT);
    halLcdPrintLineCol(val, 4,11, OVERWRITE_TEXT);
    halLcdPrintLineCol("V", 4,16, OVERWRITE_TEXT);	
	adc_value=0;
	soc=50+78.125*(voc-12.20);
	soc*=10080;
	conv(soc/100.8);
	halLcdPrintLine("Recalib Soc :", 5, OVERWRITE_TEXT);
    halLcdPrintLineCol(val, 5,12, OVERWRITE_TEXT);
 //Reconfiguring the Recalibration timing
	RTCAHOUR|=BIT7;
	RTCAMIN|=BIT7;
	}	
 RTCCTL01&=~RTCAIFG;	  	
 flag0=flag1=flag2=flag3=flag4=0;
  	 
}
void clockpage()
{
  halLcdClearScreen();
  CLOCK_FLAG=1;
  MENU_FLAG=0;
  RTCCTL01 |= RTCHOLD;
  if(hourFlag==1)
  {
  halLcdPrintLineCol(&time[HOUR10], 4,3, INVERT_TEXT|OVERWRITE_TEXT);
  halLcdPrintLineCol(&time[COLON2], 4,5, OVERWRITE_TEXT);
  }
  if(minFlag==1)
  {
  	 halLcdPrintLineCol(&time[HOUR10], 4,3, OVERWRITE_TEXT);
  	halLcdPrintLineCol(&time[MINUTE10], 4,6, INVERT_TEXT|OVERWRITE_TEXT);
   halLcdPrintLineCol(&time[COLON1], 4,8, OVERWRITE_TEXT);
  }
    if(secFlag==1)
  {
  	 halLcdPrintLineCol(&time[HOUR10], 4,3, OVERWRITE_TEXT);
  	halLcdPrintLineCol(&time[SECOND10], 4,9, INVERT_TEXT|OVERWRITE_TEXT);
    }
    flag0=0;
    flag1=flag2=flag3=0,flag4=0;
}
void recalib_init()
  {
  	 ALARM_MINUTE= RTCMIN;
     ALARM_HOUR =RTCHOUR;
  	 RTCAHOUR|=BIT7;
	RTCAMIN|=BIT7;
  	 RTCAHOUR|=ALARM_HOUR;
	RTCAMIN|= ALARM_MINUTE;
	socpage();
  }
/********************************************************
 * Funtion to measure power derived from the solar panel*
 * ******************************************************/
void powerPage()
{
	halLcdClearScreen();
	while(!(ADC12IFG&BIT2));
    counter=ADC12MEM2_L;
    solar_current=counter|ADC12MEM2_H<<8;
 	while(!(ADC12IFG&BIT0));
    counter=ADC12MEM0_L;
    voltage=counter|ADC12MEM0_H<<8;
     halLcdPrintLineCol("Solar Power", 1,3, OVERWRITE_TEXT|INVERT_TEXT);
    solar_current=solar_current*0.3639;
    voltage=voltage*0.4520;
    power=(float)(voltage/100)*(float)(solar_current/100);
    
    convp(power);
    halLcdPrintLineCol(val, 2,6, OVERWRITE_TEXT);
    halLcdPrintLineCol("W", 2,10, OVERWRITE_TEXT);
	flag0=flag1=flag2=0,flag3=0,flag4=0;
}
/**********************************************************
 * Graphic display of State of charge                     *
 * ********************************************************/
 void batterypercentagepage()
{	
		
	percentage=soc/100.8;
	conv(percentage);
	percentage=percentage/1000;
if(boxFlag==1)
{	
	        // SMCLK, upmode, clear TAR
    halLcdClearScreen();
	halLcdVLine(28,33,66,PIXEL_DARK);
    halLcdVLine(115,33,66,PIXEL_DARK);
    halLcdHLine(28,115,33,PIXEL_DARK);
    halLcdHLine(28,115,66,PIXEL_DARK);
    halLcdHLine(115,120,54,PIXEL_DARK);
    halLcdHLine(115,120,44,PIXEL_DARK);
    halLcdVLine(120,44,54,PIXEL_DARK);
    display_bolt(60,50);
    boxFlag=0;
}
    conv(soc/100.8);
    halLcdPrintLine(val, 1, OVERWRITE_TEXT);
	display_percentage(45,17);
	
   if(percentage<10)
	{
	halLcdImage(BLANK,10,24,38,38);
    halLcdImage(BATTERY, percentage, 24, 38, 38);
   }
//	else
//	{
//		halLcdPrintLine("Over Voltage", 1, OVERWRITE_TEXT);
//	}
	currentFlag=1;
	 if(chargeFlag==1)
	 {
	  display_bolt(60,50);
	  halLcdPrintLine("           ", 6, OVERWRITE_TEXT);
	  halLcdPrintLine("Charging", 6, OVERWRITE_TEXT);
	 }
     if(dischargeFlag==1)
	 halLcdPrintLine("Discharging", 6, OVERWRITE_TEXT);
	 flag2=0;  flag1=flag2=0;
	 flag3=0,flag4=0;
} 
 void batteryInit()
 {
 	TA1CCTL0 = CCIE;                          // CCR0 interrupt enabled
 TA1CCR0 = 3000;
 TA1CTL = TASSEL_1 + MC_1 + TACLR+ID_3; 
 batterypercentagepage();
 }

/********************************************************************
 * Displays User Interface Menu                                     *
 * ******************************************************************/
void menu_initi(int a)
{
	halLcdClearScreen();
halLcdPrintLine("        BMS:      ", 1, OVERWRITE_TEXT);
halLcdPrintLine("1.Start", 2, OVERWRITE_TEXT);
halLcdPrintLine("2.Recalibration", 3,  OVERWRITE_TEXT);
halLcdPrintLine("3.Clock", 4, OVERWRITE_TEXT);
halLcdPrintLine("4.Battery state",5,OVERWRITE_TEXT);
halLcdPrintLine("5.Power Flow",6,OVERWRITE_TEXT);


switch(a)
{
	case 1:
	halLcdPrintLine("1.Start", 2, INVERT_TEXT|OVERWRITE_TEXT);
	break;
	case 2:
	halLcdPrintLine("2.Recalibration", 3, INVERT_TEXT|OVERWRITE_TEXT);
	break;
	case 3:
	halLcdPrintLine("3.Clock", 4, INVERT_TEXT|OVERWRITE_TEXT);
	break;
	case 4:
	halLcdPrintLine("4.Battery state", 5, INVERT_TEXT|OVERWRITE_TEXT);
	break;
	case 5:
	halLcdPrintLine("5.Power Flow", 6, INVERT_TEXT|OVERWRITE_TEXT);
	break;
}
 
}
/******************************************************************************
 * Initialises hardware Interrupts for Experimenter Board's Joystick operation*
 ******************************************************************************/
void initi()
{
	P2DIR=0x00;
	P2REN|=0xFF;
	P2OUT|=0xFF;
	P2IE|=0xFF;
	P2IES|=0xFF;
	
	
}
/*******************************************************************************
 * Configures Micro-controller's clock frequency to 25Mhz                      *
 *******************************************************************************/
void freq_setup()
{
	 
	UCSCTL3 = SELREF_2;                       // Set DCO FLL reference = REFO
    UCSCTL4 |= SELA_2;  
    UCSCTL0 = 0x0000;                         // Set lowest possible DCOx, MODx
    UCSCTL1 = DCORSEL_5;                      // Select DCO range 50MHz operation
    UCSCTL2 = FLLD_1 + 762;                       // Set ACLK = REFO
   do
  {
    UCSCTL7 &= ~(XT2OFFG + XT1LFOFFG + XT1HFOFFG + DCOFFG);
    SFRIFG1 &= ~OFIFG;                      // Clear fault flags
  }while (SFRIFG1&OFIFG);   
}
int mod( int a)
{
	if(a<0)
	{
	a*=-1;
	return a;
	}
	else
	return a;
}
/*******************************************************************************************
 * Main function where Frequency of the clock, LCD, USB, RTC are initialised and configured*
 *******************************************************************************************/ 
void main()
{
	
	WDTCTL = WDTPW + WDTHOLD;                 // Stop WDT
	P5DIR|=0x01;
	P5OUT|=0x01;
	P5DIR|=0x02;
	P5OUT|=0x02;
	freq_setup();
    halLcdInit();
    halLcdBackLightInit();
    halLcdSetBackLight(0xCF);
    halLcdSetContrast(100);
    halUsbInit();
    initi();
    adc_setup();
    P10DIR|=0x01;
    P10OUT |=0x01;
    P1DIR|=0x01;
    setupRTC();
    halLcdClearScreen();
    menu_initi(count);
    power_interrupt();
    __bis_SR_register( GIE);
    while(1)
    {
    updateDigitalClock();	
    halLcdPrintLineCol(&time[HOUR10], 8, 0,  OVERWRITE_TEXT);
    ADC12CTL0 |= ADC12SC;  
	if(flag!=0)
       	{
       	if ( prev!=count)
       	{
       	menu_initi(count);
       	prev=count;}
       	}else
       	{
       		if(count==1&&flag0!=0)
       		start();
       		if(count==2&&flag1!=0)
       		recalib_init();
       		if(count==3&&flag2!=0)
       		clockpage();
       		if(count==4&&flag3!=0)
       		batteryInit();
       		if(count==5&&flag4!=0)
       		powerPage();
       	}
     
    }
   
}
/*********************************************************************************
 * Hardware Interrupt Service Routine for Experimenter board's Joystick operation*
 *********************************************************************************/
#pragma vector=PORT2_VECTOR
__interrupt void Port2_ISR(void)
{
	   
switch(P2IFG)
{
case 0x08://select
    flag=0;

//flag3=1;
     break;
case 0x10://up
    P1OUT|=0x03;
      if(  CLOCK_FLAG==1)
      {
      	if(hourFlag==1)
      	hourAdd();
        if(secFlag==1)
        secondAdd();
        if(minFlag==1)
        minuteAdd();
        updateDigitalClock();
      	clockpage();
      	} 
      else if(MENU_FLAG==1)
      { 	
             if(count<=1)
                  count=5;
             else
                  count--;
      }
      	break;
case 0x20://down
  if(  CLOCK_FLAG==1)
      {
      	if(hourFlag==1)
      	hourSub();
        if(secFlag==1)
        secondSub();
        if(minFlag==1)
        minuteSub();
           	updateDigitalClock();
      	clockpage();
       }
    else if(MENU_FLAG==1)
    {
    if(count>=5)
    count=1;
    else
    count++;
    }
    break;
case 0x40://s1
     P1OUT ^=0x02;
     flag=1;
     flag0=flag1=flag2=flag3=flag4=1;
     count=1;
     RTCCTL01 &= ~RTCHOLD;
     prev=0;
     CLOCK_FLAG=0;
     MENU_FLAG=1;
     TA1CCTL0 = CCIE;
     TA1R=0x00;
     TA1CTL&=~TAIE;
     TA1CCTL0&=~CCIE;
     currentFlag=1;
     break;
case 0x80://s2
     P1OUT^=0x01;
     break;
case 0x02: //left
  if(CLOCK_FLAG==1)
  {
if(hourFlag==1)
{
	secFlag=1;
	minFlag=0;
	hourFlag=0;
	
}
else if(secFlag==1)
{
	secFlag=0;
	minFlag=1;
	hourFlag=0;
}
else if(minFlag==1)
{
	secFlag=0;
	minFlag=0;
	hourFlag=1;
	
}

clockpage();	
  }
break;
case 0x04: //right
  if(  CLOCK_FLAG==1)
      {
if(hourFlag==1)
{
	secFlag=0;
	minFlag=1;
	hourFlag=0;
	
}else if(minFlag==1)
{
	secFlag=1;
	minFlag=0;
	hourFlag=0;
}
else if(secFlag==1)
{
	secFlag=0;
	minFlag=0;
	hourFlag=1;
}
clockpage();

      }
break;
}
    TA1R=0x00;
    P2IFG=0;
    boxFlag=1;
  }

 #pragma vector=TIMER1_A0_VECTOR
__interrupt void TIMER1_A0_ISR(void)
{
	batterypercentagepage();
	TA1CTL&=~TAIFG;
	TA1R=0x00;
    flag3=1;
}   
/***********************************************************************
 * Uart Interrupt Service Routine for data transmission                *
 ***********************************************************************/  
#pragma vector=USCI_A1_VECTOR
__interrupt void USCI_A1_ISR(void)
{
	halUsbSendChar(UCA1RXBUF);
	while(!(ADC12IFG&BIT2));                        //Sampling Solar current
    counter=ADC12MEM2_L;
    solar_current=counter|ADC12MEM2_H<<8;        
 	while(!(ADC12IFG&BIT0));
    counter=ADC12MEM0_L;
    voltage=counter|ADC12MEM0_H<<8;                 //Sampling Battery Voltage
    halLcdPrintLine("Solar Power", 1, OVERWRITE_TEXT);
    solar_current=solar_current*0.3639;
    voltage=voltage*0.4520;
if(UCA1RXBUF=='x')
{
	conv(soc/79.2);
	halUsbSendString(val,strlen(val));
}
if(UCA1RXBUF=='y')
{
	conv(voltage);
	  halUsbSendString(val,strlen(val));
}
if(UCA1RXBUF=='z')
{
	  conv(solar_current);
	  halUsbSendString(val,strlen(val));
}
 
    P1OUT^=0x01;
}
/*******************************************************************************
 * TIMERB Interrupt Service Routine for Coulomb counting process               *
// * ***************************************************************************/
#pragma vector=TIMERB0_VECTOR
__interrupt void TIMERB0_ISR (void)
{
	
	TB0R=0x00;
	TB0CTL&=~TBIFG;
    while(!(ADC12IFG&BIT1));
    coulomb=ADC12MEM1_L;
    coulomb=coulomb|ADC12MEM1_H<<8;             //Sampling Battery Current
    while(!(ADC12IFG&BIT0));                    
    voltage=ADC12MEM0_L;
    voltage=voltage| ADC12MEM0_H<<8;            //Sampling Battrery Voltage
    coulomb-=1849;
    if(coulomb<0)
     {
 	dischargeFlag=1;
 	chargeFlag=0;
     }
    else 
     {
    chargeFlag=1;
    dischargeFlag=0;
	 }
   soc+=(double)coulomb*0.0803222;                 //SoC addition 
 if(soc>633600)
	{
    P5OUT|=0x02;      //Relay off
	prev_state=1;     //Relay off
	}		
//Switching Strategy		
 else if(soc<475200)
	{
	P5OUT&=~0x02;      //Relay on
	prev_state=0;      //Relay on
	}
 else if(prev_state==0)//Relay on
    P5OUT&=~0x02;
 else if(prev_state==1) //Relay off	
	P5OUT|=0x02;
	coulomb=mod(coulomb);
    current=(double)coulomb*0.0803222;
    conv(current*10);
if(currentFlag==0)
{
	halLcdPrintLine("Current", 2, OVERWRITE_TEXT);
    halLcdPrintLineCol(val, 2,11, OVERWRITE_TEXT);
    halLcdPrintLineCol("A", 2,15, OVERWRITE_TEXT);
    
}
     P10OUT|=0x01;
     P1OUT^=0x01;
}
/***************************************************************************
 * Real Time Clock Alarm Interrupt Service Routine to Intiate recalibration* 
 * Process.                                                                *
// *************************************************************************/

#pragma vector=RTC_VECTOR

__interrupt void handle_rtc_interrupt(void)
{
	switch(__even_in_range(RTCIV,8))
    {  
    case 6:
    P1OUT^=0x01;
    socpage();
    break;
    }//switch
}
/************************************************************************
 * Port 1 Interrupt Service Routine to Monitor Power Status in the grid *
 ************************************************************************/
#pragma vector=PORT1_VECTOR
__interrupt void Port1_ISR(void)
   {
	P1OUT^=0x01;
    P1IFG&=~0x10; 
    
    //power_interrupt();
	}
