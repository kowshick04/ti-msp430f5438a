#include "msp430x54x.h"
#include "hal_lcd.h"
#include "string.h"
#include "math.h"
char val[]="XXXX\0";
void SetVcoreUp (unsigned int level);
int i=1;
int voltage,current,counter;
int conv(int adc_val)
{    unsigned char thousands = '0'; 
	 unsigned char hundreds = '0';           // Init to '0' to code in ASCII
    unsigned char tens = '0';
    unsigned char ones = '0';
   while (adc_val >= 1000){                 // Calculate hundreds' place
       thousands++;
        adc_val -= 1000;
    }
    while (adc_val >= 100){                 // Calculate hundreds' place
        hundreds++;
        adc_val -= 100;
    }
   // if (hundreds == '0') hundreds = ' ';    //eliminate preceding zero, if any
    while (adc_val >= 10){                  // Calculate tens' place
        tens++;
        adc_val -= 10;
    }
    ones += adc_val; 
    val[3]= ones;
    val[2]=tens;
    val[1]=hundreds;
    val[0]=thousands;
    return 0;
}
void freq_setup()
{
	 SetVcoreUp (0x01);
 SetVcoreUp (0x02);  
 SetVcoreUp (0x03);
	UCSCTL3 = SELREF_2;                       // Set DCO FLL reference = REFO
  UCSCTL4 |= SELA_2;  
  UCSCTL0 = 0x0000;                         // Set lowest possible DCOx, MODx
  UCSCTL1 = DCORSEL_5;                      // Select DCO range 50MHz operation
  UCSCTL2 = FLLD_1 + 762;                       // Set ACLK = REFO
   do
  {
    UCSCTL7 &= ~(XT2OFFG + XT1LFOFFG + XT1HFOFFG + DCOFFG);
                                            // Clear XT2,XT1,DCO fault flags
    SFRIFG1 &= ~OFIFG;                      // Clear fault flags
  }while (SFRIFG1&OFIFG);   
}
void SetVcoreUp (unsigned int level)
{
  // Open PMM registers for write
  PMMCTL0_H = PMMPW_H;              
  // Set SVS/SVM high side new level
  SVSMHCTL = SVSHE + SVSHRVL0 * level + SVMHE + SVSMHRRL0 * level;
  // Set SVM low side to new level
  SVSMLCTL = SVSLE + SVMLE + SVSMLRRL0 * level;
  // Wait till SVM is settled
  while ((PMMIFG & SVSMLDLYIFG) == 0);
  // Clear already set flags
  PMMIFG &= ~(SVMLVLRIFG + SVMLIFG);
  // Set VCore to new level
  PMMCTL0_L = PMMCOREV0 * level;
  // Wait till new level reached
  if ((PMMIFG & SVMLIFG))
    while ((PMMIFG & SVMLVLRIFG) == 0);
  // Set SVS/SVM low side to new level
  SVSMLCTL = SVSLE + SVSLRVL0 * level + SVMLE + SVSMLRRL0 * level;
  // Lock PMM registers for write access
  PMMCTL0_H = 0x00;
}
void adc_setup()
{
 P6SEL |= 0x80;  
 P7SEL|=0x20;
                            // Enable A/D channel inputs
  ADC12CTL0 = ADC12ON+ADC12MSC+ADC12SHT0_2; // Turn on ADC12, set sampling time
  ADC12CTL1 = ADC12SHP+ADC12CONSEQ_3;       // Use sampling timer, single sequence
  ADC12MCTL0 = ADC12SREF_0+ADC12INCH_0;                 // ref+=AVcc, channel = A0
  ADC12MCTL1 =ADC12SREF_0+ ADC12INCH_13;                 // ref+=AVcc, channel = A1
           // Turn on ADC12, set sampling time
    
   ADC12CTL2 = ADC12RES_2;                   // Use sampling timer
                   
  ADC12CTL0 |= ADC12ENC;                   // Enable conversions
                            
}
void pwm()
{
	
	TBCCTL0 = CCIE;                           // CCR0 interrupt enabled
  TBCCR0 = 50000;
  TBCTL = TBSSEL_2 + MC_2 + TBCLR;          // SMCLK, contmode, clear TBR
  TA1CTL = TASSEL_2 + MC_1 + TACLR;
  P7DIR |= 0x08;                            // P2.2 and P2.3 output
  P7SEL |= 0x08;                            // P2.2 and P2.3 options select
  TA1CCR0 = 1750;  
   TA1CCTL2 = OUTMOD_7;                      // CCR2 reset/set
  TA1CCR2 = 128;                      // CCR1 reset/set
                        
}
int main(void)
{
 WDTCTL = WDTPW + WDTHOLD;                 // Stop WDT
  P1DIR |= 0x01; 
   halLcdBackLightInit();
    halLcdSetBackLight(0xFF);
    
    
    halLcdClearScreen();
  __bis_SR_register( GIE);
  freq_setup(); 
    adc_setup(); 
  pwm();

  
 while(1)
 {
 	 
 	 ADC12CTL0 |= ADC12SC;  
 	 

 }
                             
  return 0;

        // Enter LPM0, enable interrupts
                         // For debugger
}

 //Timer B0 interrupt service routine
#pragma vector=TIMER0_B0_VECTOR
__interrupt void TIMERB0_ISR (void)
{
	
	
	P1OUT^=0x01;
	halLcdPrintLine("var", 3, OVERWRITE_TEXT);
	_delay_cycles(1000000);
while(!(ADC12IFG&BIT0));
counter=ADC12MEM0_L;
voltage =counter| ADC12MEM0_H<<8;
conv(voltage);


  while(!(ADC12IFG&BIT1));
counter=ADC12MEM1_L;
 current=counter|ADC12MEM1_H<<8;
 
 

	               // Add Offset to CCR0 [Cont mode]
                    
}

