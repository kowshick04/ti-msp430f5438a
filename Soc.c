

#include "msp430x54x.h"
#include "hal_lcd.h"
#include "string.h"
volatile int i=0,counter=0;
char val[]="XXXX\0";
int conv(int adc_val)
{    unsigned char thousands = '0'; 
	 unsigned char hundreds = '0';           // Init to '0' to code in ASCII
    unsigned char tens = '0';
    unsigned char ones = '0';
   while (adc_val >= 1000){                 // Calculate hundreds' place
       thousands++;
        adc_val -= 1000;
    }
    while (adc_val >= 100){                 // Calculate hundreds' place
        hundreds++;
        adc_val -= 100;
    }
   // if (hundreds == '0') hundreds = ' ';    //eliminate preceding zero, if any
    while (adc_val >= 10){                  // Calculate tens' place
        tens++;
        adc_val -= 10;
    }
    ones += adc_val; 
    val[3]= ones;
    val[2]=tens;
    val[1]=hundreds;
    val[0]=thousands;
    return 0;
}
void display();
void timer_setup();
void adc_setup();
void freq_setup();
void display()
{
	halLcdPrintLine(val, 4, OVERWRITE_TEXT);  
	halLcdPrintLine("Adc_value", 3, OVERWRITE_TEXT); 
}
void freq_setup()
{
	UCSCTL3 = SELREF_2;                       // Set DCO FLL reference = REFO
  UCSCTL4 |= SELA_2;  
  UCSCTL0 = 0x0000;                         // Set lowest possible DCOx, MODx
  UCSCTL1 = DCORSEL_5;                      // Select DCO range 50MHz operation
  UCSCTL2 = FLLD_1 + 399;                       // Set ACLK = REFO
   do
  {
    UCSCTL7 &= ~(XT2OFFG + XT1LFOFFG + XT1HFOFFG + DCOFFG);
                                            // Clear XT2,XT1,DCO fault flags
    SFRIFG1 &= ~OFIFG;                      // Clear fault flags
  }while (SFRIFG1&OFIFG);   
}
void timer_setup()
{
	 TA1CCTL0 = CCIE;                          // CCR0 interrupt enabled
  TA1CCR0 = 50000;
  TA1CTL = TASSEL_1 + MC_1 + TACLR+ID_0;         // SMCLK, upmode, clear TAR
}
void adc_setup()
{
	P6SEL |= 0x80;                            // Enable A/D channel A0
  ADC12CTL0 = ADC12ON+ADC12SHT0_0+ADC12MSC;          // Turn on ADC12, set sampling time
  ADC12CTL1 = ADC12SHP+ADC12CONSEQ_2 + ADC12SSEL_0;  
  ADC12CTL2 = ADC12RES_2;                   // Use sampling timer
        ADC12MCTL0 = ADC12SREF_0+ADC12INCH_7;            // Vr+ = VeREF+ (ext) and Vr-=AVss
  ADC12CTL0 |= ADC12ENC;                    // Enable conversions
                            
}
	
void main(void)
{
  WDTCTL = WDTPW + WDTHOLD;                 // Stop WDT
           halLcdInit();
    halLcdBackLightInit();
    halLcdSetBackLight(0xFF);
    halLcdClearScreen();
    P1DIR |= 0x01; 
  P10DIR |= 0x01; 
  P10OUT |=0x01;                           // P1.0 output
 timer_setup();
 adc_setup();
 freq_setup();                 // enable interrupt
while(1)
  {
    ADC12CTL0 |= ADC12SC;                   // Start convn - software trigger
        display();
    __bis_SR_register( GIE);     // Enter LPM4, Enable interrupts
    __no_operation();     
                // For debugger
  }
}

// Timer_A3 Interrupt Vector (TAIV) handler
#pragma vector=TIMER1_A0_VECTOR
__interrupt void TIMER1_A0_ISR(void)
{
 
     P1OUT ^= 0x01;   
               // overflow
    while(!(ADC12IFG&BIT0));
counter=ADC12MEM0_L;
 counter=counter| ADC12MEM0_H<<8;
		conv(counter);


  
}

	

